from typing import Union

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from src.database import Base


class Association(Base):
    __tablename__ = "association_table"
    recipe_id = Column(Integer, ForeignKey("recipe_detail.id"), primary_key=True)
    ingredient_id = Column(Integer, ForeignKey("ingredient.id"), primary_key=True)


class Recipe(Base):
    __tablename__ = "recipe_detail"
    id: int = Column(Integer, primary_key=True, index=True)
    name: str = Column(String, index=True)
    cooking_time: int = Column(Integer, index=True)
    description: str = Column(String)
    views_count: int = Column(Integer, default=0)
    ingredients: Union[list, tuple] = relationship(
        "Ingredient",
        secondary="association_table",
        lazy="selectin",
        uselist=True,
        collection_class=list,
    )


class Ingredient(Base):
    __tablename__ = "ingredient"
    id = Column(Integer, primary_key=True, index=True)
    product = Column(String, index=True)
    recipe: Column = relationship(
        "Recipe", secondary="association_table", uselist=True, collection_class=list
    )
