FROM python:3.10

COPY ./src ./src
COPY requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

EXPOSE 5000
ENTRYPOINT ["uvicorn", "--host", "0.0.0.0", "--port", "5000", "src.main:app"]