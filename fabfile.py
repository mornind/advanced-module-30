import os

from fabric import Connection, task


@task
def deploy(ctx):
    with Connection(
        os.environ['HOST'],
        os.environ['USERNAME'],
        connect_kwargs={'password': os.environ['PASSWORD']}
    ) as c:
        with c.cd('./advanced-module-30'):
            c.run('docker-compose down')
            c.run('git pull origin main --rebase')
            c.run('docker-compose up --build -d')
