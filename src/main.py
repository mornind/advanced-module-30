from typing import Any, List, Union

from fastapi import Depends, FastAPI, Path
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from src.database import Base, async_session, engine
from src.models import Recipe
from src.schemas import RecipeDetailed, RecipeInList

app = FastAPI()


async def get_db():
    db = async_session()
    try:
        yield db
    finally:
        await db.close()


@app.on_event("startup")
async def startapp():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown(session: AsyncSession = Depends(get_db)):
    await session.close()
    await engine.dispose()


async def make_good_recipe(recipe: Recipe) -> Recipe:
    return Recipe(
        id=int(recipe.id),
        name=str(recipe.name),
        cooking_time=int(recipe.cooking_time),
        description=str(recipe.description),
        views_count=int(recipe.views_count),
        ingredients=list(ingr.product for ingr in list(recipe.ingredients)),
    )


@app.get(
    "/recipes/",
    response_model=List[RecipeInList],
    summary="Get list of recipes",
    description="Get full list of recipes from database,"
    "sorted by their popularity descending and cooking time ascending."
    "Each recipe info will include it's name, "
    "views count and cooking time in minutes.",
)
async def get_all_recipes(
    session: AsyncSession = Depends(get_db),
) -> List[Recipe]:
    recipes_request = await session.execute(
        select(Recipe).order_by(Recipe.views_count.desc()).order_by(Recipe.cooking_time)
    )

    return recipes_request.scalars().all()


@app.get(
    "/recipes/{recipy_id}",
    response_model=RecipeDetailed,
    summary="Get detailed recipe info",
    description="Shows detailed info about exact recipe "
    "with given recipe_id."
    "Includes name of the recipe, "
    "it's cooking time, list of needed ingredients "
    "and full description of cooking process",
)
async def get_one_recipy(
    session: AsyncSession = Depends(get_db),
    recipy_id: int = Path(title="Id of the recipe to get"),
) -> Recipe:
    recipy_request = await session.execute(
        select(Recipe).filter(Recipe.id == recipy_id)
    )
    recipy: Union[Recipe, Any] = recipy_request.scalars().first()
    recipy.views_count += 1
    await session.execute(
        update(Recipe)
        .where(Recipe.id == recipy_id)
        .values(views_count=recipy.views_count)
    )
    await session.commit()

    return await make_good_recipe(recipy)
